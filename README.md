# addr

This repo is a small math interpreter in Rust.  I am learning about compilers / interpreters and this was a way for me to try and internalize
the working of the scanner and more importantly a parser using recursive decent.

If you see anything here that looks like it could be done in a better way, feel free to shoot me a message.