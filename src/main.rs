mod expr;
mod parser;
mod scanner;
mod token;

use colored::*;
use std::io::{stdin, stdout, Write};

use expr::{Expr, Literal, Operator};
use parser::Parser;
use scanner::Scanner;

fn main() {
    // Creating a REPL that takes simple operations like 1 + 2 * 3 - 4 and returns the floating point value of 3.0
    // Will implicitly use f64 for all numeric data types.

    println!("{}", "~ Welcome to the addr REPL! ~".purple());
    println!(
        "{}",
        "Type your math expression and enter to evaluate.".purple()
    );

    loop {
        let mut input = String::new();

        print!("addr> ");
        stdout().flush().unwrap();

        match stdin().read_line(&mut input) {
            Ok(_) => (),
            Err(e) => eprintln!("Error occurred: {}", e),
        }

        run(&input);
    }
}

fn run(src: &str) {
    let mut scanner = Scanner::new(&src);
    let tokens = scanner.scan_tokens();
    let parser: Parser = Default::default();

    match parser.parse(&tokens) {
        Ok(expr) => println!("result: {}", eval(expr).to_string().yellow()),
        Err(err) => println!("Somehow got an error: {:?}", err),
    };
}

fn eval(expr: Expr) -> f64 {
    match expr {
        Expr::Binary(lexpr, op, rexpr) => {
            let l = eval(*lexpr);
            let r = eval(*rexpr);

            match op {
                Operator::Plus => l + r,
                Operator::Minus => l - r,
                Operator::Star => l * r,
                Operator::Slash => l / r,
            }
        }
        Expr::Literal(literal) => match literal {
            Literal::NumberLiteral(val) => val,
        },
        Expr::Empty => {
            println!("{}", "Please enter an expression to evaluate!".yellow());
            0.0
        }
        _ => unimplemented!(),
    }
}
