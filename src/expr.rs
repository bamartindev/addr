#[derive(Debug)]
pub enum Literal {
    NumberLiteral(f64),
}

#[derive(Debug)]
pub enum Operator {
    Plus,
    Minus,
    Star,
    Slash,
}

#[derive(Debug)]
pub enum Expr {
    Empty,
    Literal(Literal),
    Binary(Box<Expr>, Operator, Box<Expr>),
    Grouping(Box<Expr>),
}
