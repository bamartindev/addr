/*
 * TODO:
 *  Create a parser that returns a single Expr to later evaluate.
 *  Update code to use iterator somehow instead of keeping track of explicit index?
 */

/*
 *  Recursive decent parser (TODO: Lookup other types of parsing online and in book)
 *
 *  Order of least -> greatest precedence
 *  1. Addition / Subtration
 *  2. Multiplication / Division
 *  3. Number
 *  4. Grouping
 */

use crate::expr::{Expr, Literal as ExprLiteral, Operator};
use crate::token::{Literal as TokenLiteral, Token, TokenType};

use std::iter::Peekable;

#[derive(Default)]
pub struct Parser {}

impl Parser {
    // TODO: Create a Parse Error enum or something to replace ()
    pub fn parse(&self, tokens: &[Token]) -> Result<Expr, ()> {
        let mut peekable = tokens.iter().peekable();

        while let Some(result) = self.parse_expression(&mut peekable) {
            match result {
                Ok(expr) => {
                    return Ok(expr);
                }
                Err(err) => {
                    println!("Error: {:?}", err);
                }
            }
        }

        Ok(Expr::Empty)
    }

    fn parse_expression<'a, I>(&self, tokens: &mut Peekable<I>) -> Option<Result<Expr, ()>>
    where
        I: Iterator<Item = &'a Token>,
    {
        self.addition(tokens)
    }

    fn addition<'a, I>(&self, tokens: &mut Peekable<I>) -> Option<Result<Expr, ()>>
    where
        I: Iterator<Item = &'a Token>,
    {
        let mut expr = self.multiplication(tokens);

        while let Some(token) = tokens.peek() {
            if *token.typ() == TokenType::PLUS || *token.typ() == TokenType::MINUS {
                let operator = match token.typ() {
                    TokenType::PLUS => Operator::Plus,
                    TokenType::MINUS => Operator::Minus,
                    _ => panic!("Somehow got here?"),
                };

                tokens.next();

                if let Some(Ok(right_expr)) = self.multiplication(tokens) {
                    if let Some(Ok(left_exr)) = expr {
                        expr = Some(Ok(Expr::Binary(
                            Box::new(left_exr),
                            operator,
                            Box::new(right_expr),
                        )));
                    }
                }
            } else {
                break;
            }
        }

        expr
    }

    fn multiplication<'a, I>(&self, tokens: &mut Peekable<I>) -> Option<Result<Expr, ()>>
    where
        I: Iterator<Item = &'a Token>,
    {
        // TODO: Understand why updating expr to mut and assigning on line 91 fixed the issue.

        let mut expr = self.primary(tokens);

        while let Some(token) = tokens.peek() {
            if *token.typ() == TokenType::STAR || *token.typ() == TokenType::SLASH {
                let operator = match token.typ() {
                    TokenType::STAR => Operator::Star,
                    TokenType::SLASH => Operator::Slash,
                    _ => panic!("Somehow got here?"),
                };

                tokens.next();

                if let Some(Ok(right_expr)) = self.primary(tokens) {
                    if let Some(Ok(left_exr)) = expr {
                        expr = Some(Ok(Expr::Binary(
                            Box::new(left_exr),
                            operator,
                            Box::new(right_expr),
                        )));
                    }
                }
            } else {
                break;
            }
        }

        expr
    }

    fn primary<'a, I>(&self, tokens: &mut Peekable<I>) -> Option<Result<Expr, ()>>
    where
        I: Iterator<Item = &'a Token>,
    {
        if let Some(token) = tokens.peek() {
            match token.typ() {
                TokenType::NUMBER => {
                    if let Some(TokenLiteral::Float(n)) = token.literal() {
                        // TODO: Seems clunky, what do?
                        tokens.next();
                        return Some(Ok(Expr::Literal(ExprLiteral::NumberLiteral(*n))));
                    }
                }
                _ => (),
            }
        }

        None
    }
}
