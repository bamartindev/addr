#[derive(Debug, PartialEq)]
pub enum TokenType {
    LEFTPAREN,
    RIGHTPAREN,
    PLUS,
    MINUS,
    STAR,
    SLASH,
    NUMBER,
}

#[derive(Debug, PartialEq)]
pub enum Literal {
    Float(f64),
}

#[derive(Debug, PartialEq)]
pub struct Token {
    typ: TokenType,
    lexeme: Option<String>,
    literal: Option<Literal>,
}

impl Token {
    pub fn new(typ: TokenType, lexeme: Option<String>, literal: Option<Literal>) -> Token {
        Token {
            typ,
            lexeme,
            literal,
        }
    }

    pub fn typ(&self) -> &TokenType {
        &self.typ
    }

    pub fn literal(&self) -> &Option<Literal> {
        &self.literal
    }
}
