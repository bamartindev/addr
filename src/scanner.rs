use crate::token::{Literal, Token, TokenType};

use std::iter;
use std::str;

use colored::*;

pub struct Scanner<'a> {
    chars: iter::Peekable<str::Chars<'a>>,
}

impl<'a> Scanner<'a> {
    pub fn new(src: &'a str) -> Scanner<'a> {
        Scanner {
            chars: src.chars().peekable(),
        }
    }

    pub fn scan_tokens(&mut self) -> Vec<Token> {
        let mut tokens: Vec<Token> = Vec::new();

        loop {
            self.skip_whitespace();
            let c = self.chars.next();

            match c {
                Some(c) => match self.scan_token(c) {
                    Ok(token) => tokens.push(token),
                    Err(_) => eprintln!("{}", "wtf".red()),
                },
                None => {
                    // End of input.
                    break;
                }
            }
        }

        tokens
    }

    fn scan_token(&mut self, c: char) -> Result<Token, ()> {
        match c {
            '+' => Ok(self.simple_token(TokenType::PLUS)),
            '-' => Ok(self.simple_token(TokenType::MINUS)),
            '/' => Ok(self.simple_token(TokenType::SLASH)),
            '*' => Ok(self.simple_token(TokenType::STAR)),
            '(' => Ok(self.simple_token(TokenType::LEFTPAREN)),
            ')' => Ok(self.simple_token(TokenType::RIGHTPAREN)),
            '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0' => self.parse_number(c),
            _ => Err(()),
        }
    }

    fn simple_token(&self, typ: TokenType) -> Token {
        Token::new(typ, None, None)
    }

    fn parse_number(&mut self, c: char) -> Result<Token, ()> {
        let mut string = String::new();
        string.push(c);

        while let Some(&c) = self.chars.peek() {
            match c {
                '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0' | '.' => string.push(c),
                _ => {
                    break;
                }
            }

            self.chars.next();
        }

        // Doesn't seem right to unwrap here.  Revisit.
        let float: f64 = string.parse().unwrap();

        Ok(Token::new(
            TokenType::NUMBER,
            Some(string),
            Some(Literal::Float(float)),
        ))
    }

    fn skip_whitespace(&mut self) {
        while let Some(&c) = self.chars.peek() {
            if !c.is_whitespace() {
                return;
            }

            self.chars.next();
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scan_empty_string() {
        let expected: Vec<Token> = Vec::new();
        let mut scanner = Scanner::new("");

        assert_eq!(expected, scanner.scan_tokens())
    }

    #[test]
    fn scan_invalid_chars() {
        let input = "%^#";
        let expected: Vec<Token> = Vec::new();
        let mut scanner = Scanner::new(input);

        assert_eq!(expected, scanner.scan_tokens())
    }

    #[test]
    fn scan_valid_non_literal() {
        let input = "(+-/*)";
        let expected = vec![
            Token::new(TokenType::LEFTPAREN, None, None),
            Token::new(TokenType::PLUS, None, None),
            Token::new(TokenType::MINUS, None, None),
            Token::new(TokenType::SLASH, None, None),
            Token::new(TokenType::STAR, None, None),
            Token::new(TokenType::RIGHTPAREN, None, None),
        ];
        let mut scanner = Scanner::new(input);

        assert_eq!(expected, scanner.scan_tokens())
    }

    #[test]
    fn scan_valid_float_literals() {
        let input = "12 123. 45.6789";
        let expected = vec![
            Token::new(
                TokenType::NUMBER,
                Some("12".to_string()),
                Some(Literal::Float(12_f64)),
            ),
            Token::new(
                TokenType::NUMBER,
                Some("123.".to_string()),
                Some(Literal::Float(123_f64)),
            ),
            Token::new(
                TokenType::NUMBER,
                Some("45.6789".to_string()),
                Some(Literal::Float(45.6789_f64)),
            ),
        ];
        let mut scanner = Scanner::new(input);

        assert_eq!(expected, scanner.scan_tokens())
    }
}
